var current;
var selects;
var stuff = {
    "Окна": {
        "Rehau": {
            "Окно Стандарт": "50",
            "Окно Экскюзив": "100",
            "Окно VIP": "200"
        },
        "Ua Окна": {
            "Окно Обычное": "50",
            "Окно Супер предложение": "100",
            "Окно Первый класс": "200"
        },
        "Дом": {
            "Окно Обычное": "50",
            "Окно Экскюзив": "100",
            "Окно VIP": "200"
        },
        "Уют": {
            "Окно Открывающееся": "50",
            "Окно с ралетом": "100",
            "Окно с жалюзи": "200"
        }
    },
    "Двери": {
        "УкрДвери": {
            "Железные": "50",
            "Экскюзив": "100",
            "Комнатные двери": "200"
        },
        "Защита": {
            "Защита+": "50",
            "Защита++": "100",
            "Защита+++": "200"
        },
        "Уют": {
            "Двери хорошие": "50",
            "Двери прекрасные": "100",
            "Двери замечательные": "200"
        }
    }

};

function init() {
    selects = document.getElementsByTagName('select');
    current = {
        stuff:undefined,
        firm:undefined,
        subject:undefined,
        cost:undefined
    };
    
    
    
    
    selects[0].onchange = function() {
        current.firm = stuff[this.value];
        load(selects[1], current.firm);
        selects[1].onchange();
    };
    selects[1].onchange = function() {
        current.subject = current.firm[this.value];
        load(selects[2], current.subject);   
        selects[2].onchange();
    };
    selects[2].onchange = function() {
        var field = document.getElementsByName('cost')[0];
        field.value = current.subject[selects[2].value] + ' $';
        current.cost = field.value;
    }
    
    // load first option
    load(selects[0], stuff, false);
    
};

function load(select, objInfo, rmChilds){
    if (!(select.innerHTML[0] === '*' && select.innerHTML[select.innerHTML.length - 1] === '*')) {

        if(rmChilds || arguments.length === 2)
          removeChilds(select);    
         
        for (var key in objInfo) {
            var newOption = document.createElement('option');
            newOption.innerHTML = key.toString();
            select.appendChild(newOption);
        }

        select.removeAttribute('disabled');
    }
    
}

function removeChilds(obj){
    while(obj.firstElementChild){
        obj.removeChild(obj.firstElementChild);
    }
}

function buy(){
    if(current.firm != undefined || current.stuff || undefined&& current.subject|| undefined){
        var h1 = document.createElement('h1');
        var stuff = selects[0][selects[0].selectedIndex].value;
        var firm = selects[1][selects[1].selectedIndex].value;
        var subj = selects[2][selects[2].selectedIndex].value;
        h1.innerHTML = 'Вы купили продукт ' + stuff + ' фирмы ' + firm + ' под названием "' + subj + '"';
        removeChilds(document.getElementsByClassName('content')[0]);

        document.getElementsByClassName('content')[0].appendChild(h1);
    } 
    else 
        alert('Не всё выбрано!');
}

window.onload = init();
