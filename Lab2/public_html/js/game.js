// Объявления перменных и функций
var canvas = document.getElementById("canvas1");
var ctx = canvas.getContext("2d");

function main() {
    
    
    

    var music_babah = document.getElementById("babah");
    var music_dysh = document.getElementById("dysh");

    var bar = { x: 300, y: 300, h: 20, w: 100};
    var ball = { x: 50, y: 200, r: 2, vx: 1, vy: -1 };
    
    var obstacles = [];
    var how_many_obst;
    
    
    how_many_obst = obstacles.N = 25     ; // N - константа, а how_many_obst - нужно для определеиня победы. Вот так.
    obstacles.width = 100;
    obstacles.height = 10;
    
    

    // Исполняемый код
    createObstacles(obstacles, canvas);
    drawRect(bar, ctx);    
    drawObstacles(obstacles, ctx);
    document.body.onkeydown = controller;






    var timer = setInterval(function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        drawRect(bar, ctx);
        drawBall(ball, ctx);
        drawObstacles(obstacles, ctx)

        if (ball) { moveBall(); }
        else {
            var msg = document.getElementById('noter');
            msg.innerHTML = 'You loose :(';
            msg.style.setProperty('display', 'block');
            clearInterval(timer);
        }
        if (isWinner()) {
            var msg = document.getElementById('noter');

            msg.style.setProperty('display', 'block');
            clearInterval(timer);
        }

        function isWinner() {
            if (how_many_obst === 0) {
                return true;
            }

            return false;
        }
    }, 4);

    //Function Declaration
    function controller(e) {
        if (e.keyCode == 37 && check('left')) {
            bar.x -= bar.w / 2;
            e.preventDefault();
            
        }
        if (e.keyCode == 39 && check('right')) {
            bar.x += bar.w / 2;
            e.preventDefault();
        }
        if (e.keyCode == 38 && check('up')) {
            bar.y -= bar.h;
            e.preventDefault();
        }
        if (e.keyCode == 40 && check('down')) {
            bar.y += bar.h;
            e.preventDefault();
        }

        function check(direction) {
            switch (direction) {
                case "left":
                    if (bar.x - bar.w / 2 < 0) {
                        return false;
                    }
                    return true;
                    break;
                case "right":
                    if (bar.x + bar.w + bar.w / 2 > canvas.width) {
                        return false;
                    }
                    return true;
                    break;

                case "up":
                    if (bar.y - bar.h < 0) {
                        return false;
                    }
                    return true;
                    break;

                case "down":
                    if (bar.y + 2 * bar.h > canvas.height) {
                        return false;
                    }
                    return true;
                    break;

            }
        }
        
    }    
    function moveBall() {

        checkTheWall();
        checkTheRect();
        checkTheObstacles();

        ball.x += ball.vx;
        ball.y += ball.vy;


        function checkTheWall() {
            if (ball.x < ball.r || ball.x > canvas.clientWidth - ball.r)
                ball.vx = -ball.vx;
            if (ball.y < ball.r)
                ball.vy = -ball.vy;
            if(ball.y > canvas.clientHeight - ball.r) // коснулся нижней рамки
            {
                ball = null;
            }
        }
        function checkTheRect() {

            if (checkOnTheBar(bar) && checkBetweenTheBarEdges(bar)) {
                ball.vy = -ball.vy;
                ball.y -= ball.r / 2;
               // music_dysh.play();
            }

            if (onTheCorner(bar)) {
                ball.vy = -ball.vy;
                ball.vx = -ball.vx;
            //    music_dysh.play();
            }

            if (onTheLeftRightEdges(bar)) {
                ball.vx = -ball.vx;
            //    music_dysh.play();
            }
            
            

        }
        function checkTheObstacles() {
            for (var i = 0; i < obstacles.length; i++) {
                if (obstacles[i]) {
                    if (checkUnderTheBar(obstacles[i]) && checkBetweenTheBarEdges(obstacles[i])) {
                        ball.vy = -ball.vy;
                    //    music_babah.play();
                        how_many_obst -= 1;
                        delete (obstacles[i]);
                        break;



                    }

                    if (checkOnTheBar(obstacles[i]) && checkBetweenTheBarEdges(obstacles[i])) {
                        ball.vy = -ball.vy;
                        ball.y -= bar.h / 2;
                   //     music_dysh.play();
                        how_many_obst -= 1;
                        delete (obstacles[i]);
                        break;

                        

                    }

                    if (onTheCorner(obstacles[i])) {
                        ball.vy = -ball.vy;
                        ball.vx = -ball.vx;
                      //  music_dysh.play();
                        how_many_obst -= 1;
                        delete (obstacles[i]);
                        break;


                    }

                    if (onTheLeftRightEdges(obstacles[i])) {
                        ball.vx = -ball.vx;
                    //    music_dysh.play();
                        how_many_obst -= 1;
                        delete (obstacles[i]);
                        break;

                    }
                }
            }

            
        }

        //--declaration
        function checkOnTheBar(bar) {
            return (ball.y + ball.r == bar.y); // Если скорость будет больше 1, то правь код, лентяй!
        }
        function checkUnderTheBar(bar) {
            return bar.y + bar.h == ball.y - ball.r;
        }
        function checkBetweenTheBarEdges(bar) {
            return (bar.x <= ball.x) && (bar.x + bar.w >= ball.x);
        }
        function onTheCorner(bar) {
            return ball.x + ball.r == bar.x && ball.y + ball.r == bar.y //левый верхний край бара
                || //или
                ball.x - ball.r == bar.x && ball.y + ball.r == bar.y //правый верхний край бара
                ||
                ball.y + ball.r == bar.x && ball.y - ball.r == bar.y // левый нижний край бара
                ||
                ball.y - ball.r == bar.y && ball.x - ball.r == bar.x;//правый нижний 
        }
        function onTheLeftRightEdges(bar) {
            return (ball.y >= bar.y && ball.y <= bar.y + bar.h) && (ball.x + ball.r == bar.x || ball.x - ball.r == bar.x);
        }
    }
    

}

function createObstacles(obstacles, canvas) {
    var emptySpaceX = emptySpaceY = 15; // in PX;
    for (var i = 0, k = 0, kx = 0, ky = emptySpaceY; i < obstacles.N; k++, i++, kx += emptySpaceX) {
       
        if (emptySpaceX + obstacles.width + k * obstacles.width < canvas.width) {
            obstacles.push({
                y: ky,
                x: emptySpaceX + kx + k * obstacles.width,
                w: obstacles.width,
                h: obstacles.height
            })
        } else {
            k = 0;
            kx = 0;
            ky += emptySpaceY + obstacles.height;

            obstacles.push({
                y: ky,
                x: emptySpaceX + kx,
                w: obstacles.width,
                h: obstacles.height
            })
        }
    }
}

function drawObstacles(obstacles, ctx) {
    for (var i = 0; i < obstacles.length; i++) {
        if (obstacles[i]) {
            drawRect(obstacles[i], ctx, ['red', 'green', 'blue', 'orange', 'yellow', 'aqua', 'black'][i % 7]);
        }
    }

    
}
function drawRect(b, ctx, color) {

    ctx.fillStyle = color === undefined ? 'blue' : color;
    ctx.stroke();
    ctx.fillRect(b.x, b.y, b.w, b.h);
}
function drawBall(obj, ctx) {
    if (obj) {
        ctx.beginPath();
        ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
        ctx.fill();
    }
}


// Исполняемый код
canvas.onclick = main;

function Run(){
    canvas.style.display = 'block';
    document.getElementById('hidden').style.display = 'block';
    
}